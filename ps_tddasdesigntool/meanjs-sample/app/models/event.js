'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  _ = require('lodash'),
  EventSchema = new Schema({
    ratings: []
  });

EventSchema.methods.getTotalRatings = function(){
    var totalRatings = 0;
	
    _.each(this.ratings, function(item){
        totalRatings += item.rating;
    });
    
    return totalRatings;
};

EventSchema.methods.calculateAverageRating = function() {	
	var totalRatings = this.getTotalRatings();
    this.averageRating = totalRatings === 0 ? 0 : totalRatings / this.ratings.length;	
};

EventSchema.pre('save', function(next){
	this.calculateAverageRating();	
	next();
});

module.exports = mongoose.model('Event', EventSchema);
