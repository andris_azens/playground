'use strict';

var controller = require('../controllers/event.server.controller.js');

module.exports = function(app){
	app.route('/events').get(controller.getAllEvents);
	app.route('/events/:id').get(controller.findSingle);
};