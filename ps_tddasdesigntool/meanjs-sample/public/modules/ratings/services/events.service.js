'use strict';

var app = angular.module('ratings');

app.factory('EventsService', ['$resource', '$q', eventsService]);

function eventsService($resource, $q){
	return {
		getAllEvents: function(){
			var deffered = $q.defer();
			
			$resource('http://localhost:3000/events').query()
				.$promise
				.then(function(data){
					deffered.resolve(data);
				})
				.catch(function(error){
					deffered.reject(error);
				});
				
			return deffered.promise;
		}
	}
}