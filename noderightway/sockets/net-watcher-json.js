"use strict";

const fs = require('fs'),
	net = require('net'),
	filename = process.argv[2],
	server = net.createServer(function(connection){
		console.log("Subscriber connected");
		
		var watchJson = {type: "watching", file: filename};
		connection.write(JSON.stringify(watchJson) + '\n');
		
		let watcher = fs.watch(filename, function(){			
			var changedJson = { type: 'changed', file: filename, timestamp: Date.now()};
			connection.write(JSON.stringify(changedJson) + "\n");
		});
		
		connection.on("close", function(){
			console.log("Subscriber disconnected");
			watcher.close();
		});
	});

if (!filename){
	throw Error("No target filename was specified.");
}
	
server.listen(5432, function(){
	console.log("Listening for subscribers");
});