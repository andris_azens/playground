"use strict";

const fs = require('fs'),
	spawn = require('child_process').spawn,
	filename = process.argv[2];
	
if (!filename){
	throw Error("A file to watch must be specified!");
}

fs.watch(filename, function(event){
	console.log("File " + filename + " just changed ["+ event +"]!");

	// //let ls = spawn('ls', ['-lh', filename]);
	// let ls = spawn('cmd', ['/c', 'dir']);
	// ls.stdout.pipe(process.stdout);
});

console.log("Now watching " + filename + " for changes ...");